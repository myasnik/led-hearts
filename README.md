# Led Hearts

Turn on a LED a thousand kilometers away to tell your love you're thinking
about her.

## Adafruit IO (SERVER)

- [Sign up](https://io.adafruit.com)
- In Feeds: view all -> New Feed
- In dashboards: New Dashboard
- Gear -> Create new block -> Toggle -> Select just crested feed -> Set block title
- Save

## Android Mobile Phone (CONTROLLER)

- Install [MQTT Dash](https://play.google.com/store/apps/details?id=net.routix.mqttdash&hl=en&gl=US)
- Plus sign
- Set name (custom)
- Address: `io.adafruit.com`
- Port: `8883`
- Enable SSL/TLS
- Enable self-signed certificate
- Username: Adafruit IO username
- Password: Adafruit IO key
- Set client id (custom)
- Save (top-right)
- Go inside the object just created
- Plus sign
- Switch/button
- Set name (custom)
- Topic: `IO_USERNAME/feeds/FEED_NAME`
- Enable publishing
- Update metrinc on publish immediately
- ON: `ON`, OFF: `OFF`
- Save (top-right)
- Done, just use this as a button

## Raspberry Pi 2B (LIGHT)

<img src="./doc/rpi/sketch_rpi.png" width="200"/>

- The code and python requirements are under the folder `./rpi`
- Customize options inside `./rpi/rpi.py` script
- Add this cronjob to root crontab: 
    - `* * * * * /usr/bin/ps aux | /usr/bin/grep rpi.py | /usr/bin/grep -v grep > /dev/null; if [ $? != 0 ]; then /usr/bin/python /PATH/TO/rpi/rpi.py; fi`
    - Reviewing the script and making it editable only by root would be a great idea for security

## ESP8266 (LIGHT)

<img src="./doc/esp8266/sketch_esp8266.png" width="200"/>

- The platformio project is under the folder `./esp8266`
- Customize options inside `./esp8266/src/main.cpp` sourcecode
- The code uses WiFIManager module, so when started it will spawn a wifi
hotspot; connect to it and configure the wireless connection
    - [Further informations](https://github.com/tzapu/WiFiManager)

## Orange Pi ONE (LIGHT)

<img src="./doc/opi/sketch_opi.png" width="200"/>

- The code and python requirements are under the folder `./opi`
- Install with `python setup.py install` the submodule in `./opi`
- Customize options inside `./opi/opi.py` script
- Add this cronjob to root crontab: 
    - `* * * * * /usr/bin/ps aux | /usr/bin/grep opi.py | /usr/bin/grep -v grep > /dev/null; if [ $? != 0 ]; then /usr/bin/python3 /PATH/TO/opi/opi.py; fi`
    - Reviewing the script and making it editable only by root would be a great idea for security
