#include <Arduino.h>

#include <ESP8266WiFi.h>

//needed for library
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>

#include <MQTT.h>

#define IO_USERNAME   "IO_USERNAME"
#define IO_KEY        "IO_KEY"
#define FEED          "FEED"
#define MQTT_SERVER   "io.adafruit.com"
#define MQTT_PORT     8883
#define LED           13
#define NAME          "ESP-NAME"

WiFiClientSecure net;
MQTTClient client;

void connect() {
  net.setInsecure();

  Serial.println("Connecting...");
  while (!client.connect(NAME, IO_USERNAME, IO_KEY)) {
    Serial.print(".");
    delay(1000);
  }

  Serial.println("Connected!");

  client.subscribe(FEED);
}

void messageReceived(String &topic, String &payload) {
  Serial.println("Incoming: " + topic + " - " + payload);
  if (payload.compareTo("ON") == 0) {
    digitalWrite(LED, HIGH);
  } else if (payload.compareTo("OFF") == 0) {
    digitalWrite(LED, LOW);
  }
}

void setup() {
  Serial.begin(9600);

  pinMode(LED, OUTPUT);

  WiFiManager wifiManager;
  wifiManager.autoConnect(NAME);
  // If you want to set a password for the generated AP
  // wifiManager.autoConnect("AP-NAME", "AP-PASSWORD");
  Serial.println("WiFi Connected!");

  client.begin(MQTT_SERVER, MQTT_PORT, net);
  client.onMessage(messageReceived);

  connect();
}

void loop() {
  client.loop();
  delay(10);

  if (!client.connected()) {
    connect();
  }
}
