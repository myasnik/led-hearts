#!/usr/bin/python

import RPi.GPIO as GPIO
import sys
from Adafruit_IO import MQTTClient

ADAFRUIT_IO_KEY = "aio_KEY"
ADAFRUIT_IO_USERNAME = "user"
FEED_ID = "feed"
LED_PIN = 24

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(LED_PIN, GPIO.OUT)

def connected(client):
    print('Connected to Adafruit IO!  Listening for {0} changes...'.format(FEED_ID))
    client.subscribe(FEED_ID)

def subscribe(client, userdata, mid, granted_qos):
    print('Subscribed to {0} with QoS {1}'.format(FEED_ID, granted_qos[0]))

def disconnected(client):
    print('Disconnected from Adafruit IO!')
    sys.exit(1)

def message(client, feed_id, payload):
    print('Feed {0} received new value: {1}'.format(feed_id, payload))
    if payload == "ON":
        GPIO.output(LED_PIN, GPIO.HIGH)
    elif payload == "OFF":
        GPIO.output(LED_PIN, GPIO.LOW)

client = MQTTClient(ADAFRUIT_IO_USERNAME, ADAFRUIT_IO_KEY)

client.on_connect    = connected
client.on_disconnect = disconnected
client.on_message    = message
client.on_subscribe  = subscribe

client.connect()

client.loop_blocking()
